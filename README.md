# Test containers with InSpec

This project contains sample code for testing docker containers.

Read the blog post here: https://ephracis.com/2019/03/30/automatic-testing-of-docker-containers/